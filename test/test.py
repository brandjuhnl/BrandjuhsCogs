import discord
from discord.ext import commands
from cogs.utils import checks

#https://twentysix26.github.io/Red-Docs/red_guide_make_cog/

class MyFirstCog:
    """My custom cog that does stuff!""" #description of the cog. Will be shown on install.

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def myfirstcommand(self):
        """This does stuff!""" #description of the command that will show when used help command.

        #Your code will go here
        await self.bot.say("Hello Everyone, if you read this you witness the first few lines of code done by Brandjuh. He tries to make his own stuff for me now!")

    @commands.command()
    async def troll(self, user : discord.Member):
        """Troll a user""" #description of the command that will show when used help command.

        #Code goes here
        await self.bot.say("Hey " + user.mention + " I troll you.")

    #@commands.command(pass_context=True)
    #async def getid(self, ctx, user: discord.Member):
    #author = ctx.message.author
    #author.id # This is your author id number
    #user.id # This is the user's id
    #author.name # This is the author's id
    #user.name # This is the user's name    
    #    await self.bot.send_message(author.id)
def setup(bot):
    bot.add_cog(MyFirstCog(bot))